#include <stdint.h>

#ifndef IMAGE_H
#define IMAGE_H

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

enum image_status {
    IMAGE_OK = 0,
    IMAGE_NULL_POINTER,
    IMAGE_MEMORY_ALLOCATION_ERROR
};

enum image_status init_image(struct image *img, uint64_t width, uint64_t height);
enum image_status free_image(struct image *img);

#endif

