#include "bmp.h"
#include "transform.h"

#include <stdio.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s BMP_FILE_NAME1 BMP_FILE_NAME2\n", argv[0]);
        return 0;
    }

    //opening and checking files
    FILE *input_file = fopen(argv[1], "r");
    if (input_file == NULL) {
        perror("Ошибка открытия входного файла");
        return 1;
    }

    FILE *output_file = fopen(argv[2], "wb");
    if (output_file == NULL) {
        fclose(input_file);
        fprintf(stderr, "Error opening output file\n");
        return 1;
    }


    struct image img;
    enum read_status read_status = from_bmp(input_file, &img);


    if (read_status != READ_OK) {
        printf("READ ERROR");
        return 0;
    }

    // rotate image
    struct image rotated_img = rotate(&img);

    // write to bmp
    enum write_status write_status = to_bmp(output_file, &rotated_img);

    if (write_status != WRITE_OK) {
        printf("WRITE ERROR");
        fclose(input_file);
        fclose(output_file);
        free_image(&img);
        free_image(&rotated_img);
        return 0;
    }

    // cleaner
    fclose(input_file);
    fclose(output_file);
    free_image(&img);
    free_image(&rotated_img);

    return 0;
}
