#include "image.h"
#include <stdlib.h>

enum image_status init_image(struct image *img, uint64_t width, uint64_t height) {
    if (img == NULL) {
        return IMAGE_NULL_POINTER;
    }

    img->width = width;
    img->height = height;
    img->data = malloc(width * height * sizeof(struct pixel));

    if (img->data == NULL) {
        return IMAGE_MEMORY_ALLOCATION_ERROR;
    }

    return IMAGE_OK;
}

enum image_status free_image(struct image *img) {
    if (img == NULL) {
        return IMAGE_NULL_POINTER;
    }

    free(img->data);
    img->data = NULL;

    return IMAGE_OK;
}

