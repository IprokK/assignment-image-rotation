#include "bmp.h"

#include <stdlib.h>


enum read_status from_bmp(FILE *in, struct image *img) {

    struct bmp_header header = {0};
    size_t result = fread(&header, sizeof(struct bmp_header), 1, in);

    if (result != 1) {
        perror("Error reading BMP header");
        return READ_ERROR;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    
    img->data = malloc(img->width * img->height * sizeof(struct pixel));

    
    if (img->data == NULL) {
        perror("Error allocating memory for image data");
        return READ_ERROR;
    }

    fseek(in, header.bOffBits, SEEK_SET);

    
    int padding = (4 - (int)(img->width * sizeof(struct pixel)) % 4) % 4;

    for (int i = (int)(img->height - 1); i >= 0; --i) {
        for (int j = 0; j < img->width; ++j) {
            if (fread(&img->data[i * img->width + j], sizeof(struct pixel), 1, in) != 1) {
                free(img->data);
                return READ_ERROR;
            }
        }

        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}


void init_bmp_header(struct bmp_header *header, uint32_t width, uint32_t height) {
    header->bfType = 0x4D42;
    header->bfileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel);
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = 40;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = width * height * sizeof(struct pixel);
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}


enum write_status write_bmp_header(FILE *out, const struct bmp_header *header) {
    size_t result = fwrite(header, sizeof(struct bmp_header), 1, out);

    if (result != 1) {
        perror("Error writing BMP header");
        return WRITE_ERROR;
    }

    return WRITE_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    struct bmp_header header;
    init_bmp_header(&header, img->width, img->height);

    if (write_bmp_header(out, &header) != WRITE_OK) {
        return WRITE_ERROR;
    }

    int padding = (4 - (int)(img->width * sizeof(struct pixel)) % 4) % 4;
    
    uint8_t padding_zeros[padding];    

    for (int i = (int)(img->height - 1); i >= 0; --i) {
	    if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width) {
		return WRITE_ERROR;
	    }

	    if (padding > 0) {
		for (int p = 0; p < padding; ++p) {
		    padding_zeros[p] = 0;
		}

		if (fwrite(padding_zeros, sizeof(uint8_t), padding, out) != padding) {
		    return WRITE_ERROR;
		}
	    }
	}



    return WRITE_OK;
}


