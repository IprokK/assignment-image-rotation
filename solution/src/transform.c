#include "transform.h"

#include <stdio.h>

struct image rotate(const struct image *source) {
    struct image rotated;
    rotated.width = 0;
    rotated.height = 0;

    if (source == NULL || source->data == NULL) {
        return rotated;
    }

    init_image(&rotated, source->height, source->width);

    if (rotated.data == NULL) {
        free_image(&rotated);
        return rotated;
    }

    for (uint64_t i = 0; i < source->height; ++i) {
        for (uint64_t p = 0; p < source->width; ++p) {

            uint64_t old_index = i * source->width + p;

            uint64_t new_index = (rotated.height - p - 1) * rotated.width + i;

            if (new_index < source->height * source->width) {
                rotated.data[new_index] = source->data[old_index];
            }
        }
    }

    return rotated;
}

